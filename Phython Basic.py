Python 3.7.5 (tags/v3.7.5:5c02a39a0b, Oct 15 2019, 00:11:34) [MSC v.1916 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license()" for more information.
>>> print("************************************ QEM-5080 - Assignment Question No.1 *****************************************")
************************************ QEM-5080 - Assignment Question No.1 *****************************************
>>> Question1="Converting integer to float"
>>> print(Question1)
Converting integer to float
>>> NumberInteger=26
>>> Result=float(NumberInteger)
>>> Result
26.0
>>>
>>>
>>> print("************************************ QEM-5080 - Addition of string(higher) data type and integer(lower) datatype *****************************************")
************************************ QEM-5080 - Assignment Question No.2 *****************************************
>>> Question2="Addition of string(higher) data type and integer(lower) datatype"
>>> print(Question2)
Addition of string(higher) data type and integer(lower) datatype
>>> num_int = 123
>>> num_str = "456"
>>> num_str = int(num_str)
>>> num_sum = num_int + num_str
>>> print("Sum of num_int and num_str:",num_sum)
Sum of num_int and num_str: 579
>>> print("************************************ QEM-5080 - Assignment Question No.4 *****************************************")
************************************ QEM-5080 - Assignment Question No.4 *****************************************
>>> text='''QEM-5080 - Assignment Question No.4
input = ""GameOfThrones""
5 outputs:
a) extract from input => ""one""
b) convert (a) from ""one"" to => ""One""
c) reverse the input => ""senorhTfOemaG""
d) reverse the input case => ""gAMEoFtHRONES""
e) extract caps into one string => ""GOT"""'''
>>> print(text)
QEM-5080 - Assignment Question No.4
input = ""GameOfThrones""
5 outputs:
a) extract from input => ""one""
b) convert (a) from ""one"" to => ""One""
c) reverse the input => ""senorhTfOemaG""
d) reverse the input case => ""gAMEoFtHRONES""
e) extract caps into one string => ""GOT"""
>>> input="GameOfThrones"
>>> extract from input => ""one""
Answer: 
>>> input[9:12]
'one'
>>>convert (a) from ""one"" to => ""One""
Answer: 
>>> input[9:12].title()
'One'
>>> input[9:12].capitalize()
'One'
>>> reverse the input => ""senorhTfOemaG""
Answer: 
>>> input[::-1]
'senorhTfOemaG'
>>> reverse the input case => ""gAMEoFtHRONES""
Answer: 
>>> input.swapcase()
>>> extract caps into one string => ""GOT"""'''
Answer: 
>>> ''.join([u for u in input if u.isupper()])
'GOT'

